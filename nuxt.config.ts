// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
   devtools: { enabled: true },
    app: {
      head: {
          link: [
            {
              rel: 'stylesheet',
              href: 'https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css'
            }
          ],
          script: [
            {
              defer :true,
              src: 'https://unpkg.com/alpinejs@3.10.2/dist/cdn.min.js',
            },
          ],
      }
  },
  css: ['~/assets/css/main.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  plugins: [
    'plugins/highchart.js'
  ],
  modules: [
    'nuxt3-leaflet'
  ],
  ssr: false
})
